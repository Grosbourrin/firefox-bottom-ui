<h1 align="center">firefox-bottom-ui</h1>

<p align="center"><img src="https://user-images.githubusercontent.com/7110136/90187878-34a7e100-ddc3-11ea-89c7-8d492c43718b.gif"></img></p1>

### Steps for applying this configuration
<ol>
  <li>Type about:profiles into your urlbar and go to the page</li>  
  <li>Open the root directory folder specified on the page</li>  
  <li>Inside this folder, create a folder named "chrome"</li>  
  <li>Put the userchrome.css file from this repo into the chrome folder</li>  
  <li>Type about:config into your urlbar and go to the page</li>
  <li>Paste "toolkit.legacyUserProfileCustomizations.stylesheets" into the bar and set its value to true</li>
  <li>Go back to about:profiles and click the restart normally buton</li>
  <li>That should be it!</li>
</ol>
